package mx.ticketit.practica6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import mx.ticketit.practica6.R.id.txtDivisor
import java.math.BigDecimal
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {

    /*  Autor: Nephtali Gómez A
    *   Fecha: 29 Marzo 2020
    *   Parte 2 - Vinculación de de widgets de edit text
    * */

    lateinit var txtDivisor : EditText
    lateinit var txtDividendo : EditText
    lateinit var btnFlotante : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Enlace de cajas de texto divisor y dividendo

        txtDivisor = findViewById(R.id.txtDivisor)
        txtDividendo = findViewById(R.id.txtDividendo)


        // Parte 4 - SnackBar
        btnFlotante = findViewById(R.id.btnDividir)

        val vDivisor = findViewById<EditText>(R.id.txtDivisor)
        val vDividendo = findViewById<EditText>(R.id.txtDividendo)
        val vBtnResultado = findViewById<FloatingActionButton>(R.id.btnDividir)

        btnFlotante.setOnClickListener {


            /*

             Parte 5 - Se incorporan las siguientes validaciones:

             - Valores vacios (se muestra validación en Error del EditText
             - Valores numéros (incluyendo decimales), se limitó desde el main_activity cambiando el tipo de dato
             - Control de excepción cuando el dividendo es 0
             */

            System.out.println ("Previo a entrar en condición de valores vacíos")

            if (vDivisor.text.toString().isEmpty()) {

                System.out.println("Entra en condicion de valores vacios (divisor)")
                vDivisor.error = "El campo divisor no puede estar vacío"
            }

            if (vDividendo.text.toString().isEmpty()) {

                System.out.println("Entra en condicion de valores vacios (dividendo)")
                vDividendo.error = "Em campo dividendo no puede estar vacío"

            }

            // Parte 5 - Ajuste en tipos de dato por BigDecimal (por cantidades muy grandes superiores a valores decimales)
            // Se utiliza funcion divide por precision y tipo de dato

            // Se incorpora try /catch para controlar excepción

            // Parte 5 - Se incorpora control de excepción para división entre cero


            try {
                var divisor: BigDecimal = txtDivisor.editableText.toString().toBigDecimal()
                var dividendo: BigDecimal = txtDividendo.editableText.toString().toBigDecimal()
                var divisionDecimal = divisor.divide(dividendo,2, RoundingMode.HALF_UP)

                // Se agrega filtro a dos decimales para controlar excepción:
                // Non-terminating decimal expansion; no exact representable decimal result

                // Parte 4 - SnackBar con texto en pantalla "El resultado es"

                Snackbar.make(it,"El resultado es: "+ divisionDecimal.toString(),Snackbar.LENGTH_LONG)
                    .setAction("Action",null)
                    .show()


            } catch (e: NumberFormatException) {
                e.printStackTrace()
                Toast.makeText(this, "Ha ocurrido un error numérico",Toast.LENGTH_SHORT).show()

            }
            catch (x: ArithmeticException) {
                x.printStackTrace()
                Toast.makeText(this, "No es posible realizar una división entre cero",Toast.LENGTH_SHORT).show()

            }


        }

    }

    fun divisionNumerica (v: View) {
        var divisor: BigDecimal = txtDivisor.editableText.toString().toBigDecimal()
        var dividendo: BigDecimal = txtDividendo.editableText.toString().toBigDecimal()

        Toast.makeText(this, (divisor/dividendo).toString(),Toast.LENGTH_SHORT).show()
    }
}
